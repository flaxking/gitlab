{% set server = pillar.gitlab.server %}

gitlab_gitlab-ce:
  pkgrepo.managed:
    - humanname: gitlab_gitlab-ce
    - baseurl: https://packages.gitlab.com/gitlab/gitlab-ce/el/7/$basearch
    - gpgkey: https://packages.gitlab.com/gitlab/gitlab-ce/gpgkey https://packages.gitlab.com/gitlab/gitlab-ce/gpgkey/gitlab-gitlab-ce-3D645A26AB9FBD22.pub.gpg
    - gpgcheck: 1
    
gitlab_server_packages:
  pkg.installed:
  - name: gitlab-ce
  {%- if server.version is defined %}
  - version: {{ server.version }}
  {%- endif %}
  - require:
    - pkgrepo: gitlab_gitlab-ce

/etc/gitlab/gitlab.rb:
  file.managed:
  - source: salt://gitlab/files/gitlab.rb
  - template: jinja

gitlab_server_reconfigure:
  cmd.wait:
  - name: gitlab-ctl reconfigure
  - user: root
  - watch:
    - file: /etc/gitlab/gitlab.rb
    - pkg: gitlab_server_packages

{% if server.public_signup_enabled is defined %}
gitlab_server_public_signup:
   cmd.run:
   - name: "gitlab-rails runner 'ApplicationSetting.last.update_attributes(signup_enabled: '{{ server.public_signup_enabled|lower }}')'"
   - user: root
{% endif %}

{% if server.backup is defined %}
umask 0077; tar cfz {{ server.backup.secret_location }}/$(date "+etc-gitlab-\%s.tgz") -C / etc/gitlab/gitlab-secrets.json:
  cron.present:
    - identifier: GitLabSecretBackup
    - minute: "{{ server.backup.get('minute', '0') }}"
    - hour: "{{ server.backup.hour }}"
    - daymonth: "{{ server.backup.get('daymonth', '*') }}"
    - month: "{{ server.backup.get('month', '*') }}"
    - dayweek: "{{ server.backup.get('dayweek', '*') }}"

gitlab-rake gitlab:backup:create:
  cron.present:
    - identifier: GitLabBackup
    - minute: "{{ server.backup.get('minute', '0') }}"
    - hour: "{{ server.backup.hour }}"
    - daymonth: "{{ server.backup.get('daymonth', '*') }}"
    - month: "{{ server.backup.get('month', '*') }}"
    - dayweek: "{{ server.backup.get('dayweek', '*') }}"
{% endif %}

{% if server.mattermost.backup is defined %}
/opt/gitlab/embedded/bin/pg_dump -h /var/opt/gitlab/postgresql mattermost_production | gzip > {{ server.mattermost.backup.location }}mattermost_dbdump_$(date --rfc-3339=date).sql.gz:
  cron.present:
    - identifier: MattermostBackup
    - user: gitlab-psql
    - minute: "{{ server.mattermost.backup.get('minute', '0') }}"
    - hour: "{{ server.mattermost.backup.hour }}"
    - daymonth: "{{ server.mattermost.backup.get('daymonth', '*') }}"
    - month: "{{ server.mattermost.backup.get('month', '*') }}"
    - dayweek: "{{ server.mattermost.backup.get('dayweek', '*') }}"
{% endif %}