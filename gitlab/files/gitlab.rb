{% set server = pillar.gitlab.server %}
## GitLab configuration settings
##! This file is generated during initial installation and **is not** modified
##! during upgrades.
##! Check out the latest version of this file to know about the different
##! settings that can be configured by this file, which may be found at:
##! https://gitlab.com/gitlab-org/omnibus-gitlab/raw/master/files/gitlab-config-template/gitlab.rb.template


## GitLab URL
##! URL on which GitLab will be reachable.
##! For more details on configuring external_url see:
##! https://docs.gitlab.com/omnibus/settings/configuration.html#configuring-the-external-url-for-gitlab
{% if server.url is defined %}
external_url '{{ server.url }}'
{% else %}
external_url 'https://{{ server.server_name }}'
{% endif %}

## Legend
##! The following notations at the beginning of each line may be used to
##! differentiate between components of this file and to easily select them using
##! a regex.
##! ## Titles, subtitles etc
##! ##! More information - Description, Docs, Links, Issues etc.
##! Configuration settings have a single # followed by a single space at the
##! beginning; Remove them to enable the setting.

##! **Configuration settings below are optional.**
##! **The values currently assigned are only examples and ARE NOT the default
##!   values.**


################################################################################
################################################################################
##                Configuration Settings for GitLab CE and EE                 ##
################################################################################
################################################################################

################################################################################
## gitlab.yml configuration
##! Docs: https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/settings/gitlab.yml.md
################################################################################

### Email Settings
# gitlab_rails['gitlab_email_enabled'] = true
# gitlab_rails['gitlab_email_from'] = '{{ server.mail.from }}'
# gitlab_rails['gitlab_email_display_name'] = 'Example'
# gitlab_rails['gitlab_email_reply_to'] = '{{ server.mail.reply_to }}'
# gitlab_rails['gitlab_email_subject_suffix'] = ''

### Reply by email
###! Allow users to comment on issues and merge requests by replying to
###! notification emails.
###! Docs: https://docs.gitlab.com/ce/administration/reply_by_email.html
# gitlab_rails['incoming_email_enabled'] = true

#### Incoming Email Address
####! The email address including the `%{key}` placeholder that will be replaced
####! to reference the item being replied to.
####! **The placeholder can be omitted but if present, it must appear in the
####!   "user" part of the address (before the `@`).**
# gitlab_rails['incoming_email_address'] = "gitlab-incoming+%{key}@gmail.com"

#### Email account username
####! **With third party providers, this is usually the full email address.**
####! **With self-hosted email servers, this is usually the user part of the
####!   email address.**
# gitlab_rails['incoming_email_email'] = "gitlab-incoming@gmail.com"

#### Email account password
# gitlab_rails['incoming_email_password'] = "[REDACTED]"

#### IMAP Settings
# gitlab_rails['incoming_email_host'] = "imap.gmail.com"
# gitlab_rails['incoming_email_port'] = 993
# gitlab_rails['incoming_email_ssl'] = true
# gitlab_rails['incoming_email_start_tls'] = false

#### Incoming Mailbox Settings
####! The mailbox where incoming mail will end up. Usually "inbox".
# gitlab_rails['incoming_email_mailbox_name'] = "inbox"
####! The IDLE command timeout.
# gitlab_rails['incoming_email_idle_timeout'] = 60

### LDAP Settings
###! Docs: https://docs.gitlab.com/omnibus/settings/ldap.html
###! **Be careful not to break the indentation in the ldap_servers block. It is
###!   in yaml format and the spaces must be retained. Using tabs will not work.**

# gitlab_rails['ldap_enabled'] = false

###! **remember to close this block with 'EOS' below**
{% if server.identity is defined %}
{% set identity = server.identity %}
gitlab_rails['ldap_enabled'] = true
{% if identity.engine == 'ldap' %}
gitlab_rails['ldap_servers'] = YAML.load <<-'EOS'
  ldap:
    label: '{{ identity.get("label") }}'
    enabled: true
    host: '{{ identity.get("host") }}'
    base: '{{ identity.get("base") }}'
    port: {{ identity.get("port", 636) }}
    uid: '{{ identity.get("uid") }}'
    encryption: '{{ identity.get("method", "plain") }}'
    bind_dn: '{{ identity.get("bind_dn") }}'
    password: '{{ identity.get("password") }}'
    active_directory: '{{ identity.get("active_directory", "false")|lower }}'
    allow_username_or_email_login: {{ identity.get("allow_username_or_email_login", "true")|lower }}
    {% if identity.user_filter is defined %}
    user_filter: '{{ identity.get("user_filter") }}'
    {% endif %}
EOS
{% endif %}
{% endif %}


### Backup Settings
###! Docs: https://docs.gitlab.com/omnibus/settings/backups.html

{% if server.backup is defined %}
gitlab_rails['backup_path'] = "{{ server.backup.location }}"
{% endif %}

### GitLab application settings

#### Change the initial default admin password and shared runner registration tokens.
####! **Only applicable on initial setup, changing these settings after database
####!   is created and seeded won't yield any change.**
# gitlab_rails['initial_root_password'] = "password"
# gitlab_rails['initial_shared_runners_registration_token'] = "token"


### GitLab email server settings
###! Docs: https://docs.gitlab.com/omnibus/settings/smtp.html
###! **Use smtp instead of sendmail/postfix.**

{% if server.mail.host is defined %}
gitlab_rails['smtp_enable'] = true
gitlab_rails['smtp_address'] = "{{ server.mail.host }}"
gitlab_rails['smtp_port'] = {{ server.mail.port }}
gitlab_rails['smtp_user_name'] = "{{ server.mail.user }}"
gitlab_rails['smtp_password'] = "{{ server.mail.password }}"
gitlab_rails['smtp_domain'] = "{{ server.mail.get('domain', '') }}"
gitlab_rails['smtp_authentication'] = "login"
gitlab_rails['smtp_enable_starttls_auto'] = {{ server.mail.use_starttls|lower }}
gitlab_rails['smtp_tls'] = {{ server.mail.use_tls|lower }}
{% endif %}


################################################################################
## GitLab Mattermost
##! Docs: https://docs.gitlab.com/omnibus/gitlab-mattermost
################################################################################

{% if server.mattermost is defined %}
mattermost_external_url '{{ server.mattermost.url }}'
{% endif %}

################################################################################
# Let's Encrypt integration
################################################################################
{% if server.letsencrypt is defined %}
letsencrypt['enable'] = true
letsencrypt['contact_emails'] = ['{{ server.letsencrypt.contact_email }}']
{% endif %}