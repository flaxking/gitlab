# GitLab

This is example salt configuration for a installing and basic setup of GitLab CE
using the GitLab omnibus package. The configuration in this example is tailored
to CentOS.

## Minimal configuration
```
gitlab:
  server:
    server.url: 'http://gitlab.mydomain.com'
```


## Example extra configuration options
```
gitlab:
  server:
    server_name: 'external.mydomain.com'
    public_signup_enabled: 'false'
    letsencrypt:
      contact_email: 'me@mydomain.com'
    identity:
      engine: 'ldap'
      label: 'Active Directory'
      host: 'ad.mydomain.com'
      port: 389
      uid: 'sAMAccountName'
      bind_dn: 'CN=GitLabSVR,CN=Users,DC=ad,DC=mydomain,DC=com'
      password: 'Password1'
      active_directory: 'true'
      base: 'OU=MyUsers,DC=ad,DC=mydomain,DC=com'
    backup:
      location: '/backup'
      secret_location: '/backup'
      hour: 23
    mattermost:
      url: 'https://mattermost.mydomain.com'
      backup:
        location: '/backup'
        hour: 23
```
